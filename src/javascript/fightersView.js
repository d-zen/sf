import View from './view';
import FighterView from './fighterView';
import FighterService, {fighterService} from './services/fightersService';
import Fighter from './fighter';

var modal = document.getElementById("modal");
var close = document.getElementsByClassName("close")[0];


var fighterOne;
var fighterTwo;


close.onclick = function () {
  modal.style.display = "none";
};

window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

class FightersView extends View {
  fightersDetailsMap = new Map();

  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({tagName: 'div', className: 'fighters'});
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {

    // get from map or load info and add to fightersMap
    if (this.fightersDetailsMap.get(fighter._id) !== fighter) {
      fighterService.getFighterDetails(fighter._id)
        .then(data => {
            fighter.attack = data.attack;
            fighter.defense = data.defense;
            fighter.health = data.health;
            fighter.source = data.source;
            fighter.name = data.name;

            this.fightersDetailsMap.set(fighter._id, fighter);

          // show modal with fighter info and allow to edit health and power in this modal
          this.showModal(fighter);
          // this.startFight(fighter, fighter)
          }
        );
    };
  }

  checkFighters(){
    if (fighterTwo && fighterOne) {
      return true
    } else {
      return false
    }
  }

  showModal(fighter) {
    // const {name, source, attack, defense, health} = fighter;
    modal.getElementsByClassName('ava')[0].setAttribute('src', fighter.source);
    let specs = modal.getElementsByTagName('input');
    specs[0].value = fighter.name;
    specs[1].value = fighter.health.toString();
    specs[2].value = fighter.attack.toString();
    specs[3].value = fighter.defense.toString();
    modal.style.display = "flex";

    var first = document.getElementById("first");
    if (fighterOne) {
      first.innerText = fighterOne.name;
    }
    var second = document.getElementById("second");
    var fight = document.getElementById("fight");
    fight.style.display = 'none';

    first.onclick = function() {
      modal.getElementsByClassName('ava')[0].setAttribute('src', fighter.source);

      first.innerText = fighter.name;
      fighterOne = fighter;
      if (fighterTwo && fighterOne) {
        fight.style.display = 'block';
      }
    }

    second.onclick = function() {
      modal.getElementsByClassName('ava')[1].setAttribute('src', fighter.source);
      second.innerText = fighter.name;
      fighterTwo = fighter;
      if(fighterTwo && fighterOne) {
        fight.style.display = 'block';
      }
    }

    fight.onclick = function () {
      var chat = document.getElementById('battle-chat');
      let winner = null;

      let firstFighter = new Fighter(fighterOne);
      let secondFighter = new Fighter(fighterTwo);

      while (!winner) {
        if (firstFighter.health > 0)  {
          secondFighter.health -= firstFighter.getHitPower()-secondFighter.getBlockPower();
          console.log('second', secondFighter.health);

          // I've run into error calling createElement() of imported View
          // let log = this.createElement(
          //   {
          //     tagName: 'p',
          //     className: 'log',
          //     innerText: `${firstFighter.name} hit ${secondFighter.name}. Now ${secondFighter.name} has health!`
          //   });
          // chat.append(log);
        }
        else {
          winner = secondFighter.name;
          alert('Winner '+ winner +' !');
        }
        if (secondFighter.health > 0) {
          firstFighter.health -= secondFighter.getHitPower() - firstFighter.getBlockPower();
          console.log('first', firstFighter.health);
        } else {
          winner = firstFighter.name;
          alert('Winner '+ winner +' !');
        }
      }
    };
  }
}

export default FightersView;
