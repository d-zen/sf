class Fighter {

  constructor(fighter) {
    this.source = fighter.source;
    this.name = fighter.name;
    this.health = fighter.health;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
  }

  randomInt(min = 1, max = 2) {
    return min + Math.random() * (max - min);
    // return min + Math.random() * (max + 1 - min);
  }

  getHitPower() {
    return this.attack * this.randomInt()
  }

  getBlockPower() {
    return this.defense * this.randomInt();
  }
}

export default Fighter;
